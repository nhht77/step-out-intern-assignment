

## Things you could do to develop faster

1. [Use emmet for faster HTML development](https://www.youtube.com/watch?v=5BIAdWNcr8Y).

2. Install prettier plugin if you use VScode.

3. Make sure to have your access to the design [template](https://app.zeplin.io/project/5dfa33d21d1d8199db47cafb/screen/5e9ff95b870f547e7293beee).

4. Use hyphen or BEM for class or id naming convention.

---

## Project Structure 
```
.
├── assets
│   ├── icons
│   └── images
├── index.css
├── index.html
└── README.md
```
---

## Tips

Prettify all .html and .css files manually (change the file extension for other files)

```
npx prettier --write <link-to-file>
```

## Live example

[stepout-playground.surge.sh](stepout-playground.surge.sh)